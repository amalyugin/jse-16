package ru.t1.malyugin.tm.exception.field;

public final class ProjectIdEmptyException extends AbstractFieldException {

    public ProjectIdEmptyException() {
        super("Error! ProjectId is empty...");
    }

}