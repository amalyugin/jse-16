package ru.t1.malyugin.tm.exception.system;

public final class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Command not supported...");
    }

    public CommandNotSupportedException(String command) {
        super("Error! Command '" + command + "' not supported...");
    }

}