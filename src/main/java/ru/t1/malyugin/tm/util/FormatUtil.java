package ru.t1.malyugin.tm.util;

import java.text.DecimalFormat;

public final class FormatUtil {

    private static final long KILOBYTE = 1024;

    private static final long MEGABYTE = KILOBYTE * 1024;

    private static final long GIGABYTE = MEGABYTE * 1024;

    private static final long TERABYTE = GIGABYTE * 1024;

    private static final String SIGN_BYTE = "B";

    private static final String SIGN_MEGABYTE = "Mb";

    private static final String SIGN_KILOBYTE = "Kb";

    private static final String SIGN_GIGABYTE = "Gb";

    private static final String SIGN_TERABYTE = "Tb";

    private static final String SEPARATOR = " ";

    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.##");

    private static String floatForm(final double value) {
        return DECIMAL_FORMAT.format(value);
    }

    private static String render(final double bytes, final String sign) {
        return floatForm(bytes) + SEPARATOR + sign;
    }

    public static String formatBytes(final long bytes) {
        if (Math.abs(bytes) < KILOBYTE) return render(bytes, SIGN_BYTE);
        if (Math.abs(bytes) < MEGABYTE) return render((double) bytes / KILOBYTE, SIGN_KILOBYTE);
        if (Math.abs(bytes) < GIGABYTE) return render((double) bytes / MEGABYTE, SIGN_MEGABYTE);
        if (Math.abs(bytes) < TERABYTE) return render((double) bytes / GIGABYTE, SIGN_GIGABYTE);
        return render((double) bytes / TERABYTE, SIGN_TERABYTE);
    }

    private FormatUtil() {
    }

}