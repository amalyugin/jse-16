package ru.t1.malyugin.tm.api.controller;

public interface IProjectTaskController {

    void bindTaskToProject();

    void unbindTaskFromProject();

}