package ru.t1.malyugin.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void showTasksByProjectId();

    void showTaskById();

    void showTaskByIndex();

    void createTask();

    void clearTasks();

    void removeTaskById();

    void removeTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void startTaskById();

    void startTaskByIndex();

    void completeTaskById();

    void completeTaskByIndex();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

}