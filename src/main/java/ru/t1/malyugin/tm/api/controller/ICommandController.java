package ru.t1.malyugin.tm.api.controller;

public interface ICommandController {

    void showVersion();

    void showAbout();

    void showHelp();

    void showSystemInfo();

}