package ru.t1.malyugin.tm.repository;

import ru.t1.malyugin.tm.api.repository.ICommandRepository;
import ru.t1.malyugin.tm.constant.ArgumentConst;
import ru.t1.malyugin.tm.constant.CommandConst;
import ru.t1.malyugin.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(CommandConst.HELP, "Show command list.", ArgumentConst.HELP);

    private static final Command VERSION = new Command(CommandConst.VERSION, "Show version info.", ArgumentConst.VERSION);

    private static final Command ABOUT = new Command(CommandConst.ABOUT, "Show Author info.", ArgumentConst.ABOUT);

    private static final Command INFO = new Command(CommandConst.INFO, "Show system info.", ArgumentConst.INFO);

    private static final Command PROJECT_LIST = new Command(CommandConst.PROJECT_LIST, "Show project list.");

    private static final Command PROJECT_CREATE = new Command(CommandConst.PROJECT_CREATE, "Create new project.");

    private static final Command PROJECT_CLEAR = new Command(CommandConst.PROJECT_CLEAR, "Clear all project.");

    private static final Command PROJECT_SHOW_BY_INDEX = new Command(CommandConst.PROJECT_SHOW_BY_INDEX, "Show project by index.");

    private static final Command PROJECT_SHOW_BY_ID = new Command(CommandConst.PROJECT_SHOW_BY_ID, "Show project by id.");

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(CommandConst.PROJECT_UPDATE_BY_INDEX, "Update project by index.");

    private static final Command PROJECT_UPDATE_BY_ID = new Command(CommandConst.PROJECT_UPDATE_BY_ID, "Update project by id.");

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(CommandConst.PROJECT_REMOVE_BY_INDEX, "Remove project by index.");

    private static final Command PROJECT_REMOVE_BY_ID = new Command(CommandConst.PROJECT_REMOVE_BY_ID, "Remove project by id.");

    private static final Command PROJECT_CHANGE_STATUS_BY_INDEX = new Command(CommandConst.PROJECT_CHANGE_STATUS_BY_INDEX, "Change project status by index.");

    private static final Command PROJECT_CHANGE_STATUS_BY_ID = new Command(CommandConst.PROJECT_CHANGE_STATUS_BY_ID, "Change project status by id.");

    private static final Command PROJECT_START_BY_INDEX = new Command(CommandConst.PROJECT_START_BY_INDEX, "Start project by index.");

    private static final Command PROJECT_START_BY_ID = new Command(CommandConst.PROJECT_START_BY_ID, "Start project by id.");

    private static final Command PROJECT_COMPLETE_BY_ID = new Command(CommandConst.PROJECT_COMPLETE_BY_ID, "Complete project by id.");

    private static final Command PROJECT_COMPLETE_BY_INDEX = new Command(CommandConst.PROJECT_COMPLETE_BY_INDEX, "Complete project by index.");

    private static final Command TASK_SHOW_BY_INDEX = new Command(CommandConst.TASK_SHOW_BY_INDEX, "Show task by index.");

    private static final Command TASK_SHOW_BY_ID = new Command(CommandConst.TASK_SHOW_BY_ID, "Show task by id.");

    private static final Command TASK_UPDATE_BY_INDEX = new Command(CommandConst.TASK_UPDATE_BY_INDEX, "Update task by index.");

    private static final Command TASK_UPDATE_BY_ID = new Command(CommandConst.TASK_UPDATE_BY_ID, "Update task by id.");

    private static final Command TASK_REMOVE_BY_INDEX = new Command(CommandConst.TASK_REMOVE_BY_INDEX, "Remove task by index.");

    private static final Command TASK_REMOVE_BY_ID = new Command(CommandConst.TASK_REMOVE_BY_ID, "Remove task by id.");

    private static final Command TASK_CHANGE_STATUS_BY_INDEX = new Command(CommandConst.TASK_CHANGE_STATUS_BY_INDEX, "Change task status by index.");

    private static final Command TASK_CHANGE_STATUS_BY_ID = new Command(CommandConst.TASK_CHANGE_STATUS_BY_ID, "Change task status by id.");

    private static final Command TASK_START_BY_INDEX = new Command(CommandConst.TASK_START_BY_INDEX, "Start task by index.");

    private static final Command TASK_START_BY_ID = new Command(CommandConst.TASK_START_BY_ID, "Start task by id.");

    private static final Command TASK_COMPLETE_BY_ID = new Command(CommandConst.TASK_COMPLETE_BY_ID, "Complete task by id.");

    private static final Command TASK_COMPLETE_BY_INDEX = new Command(CommandConst.TASK_COMPLETE_BY_INDEX, "Complete task by index.");

    private static final Command TASK_BIND_TO_PROJECT = new Command(CommandConst.TASK_BIND_TO_PROJECT, "Bind Task to Project");

    private static final Command TASK_UNBIND_FROM_PROJECT = new Command(CommandConst.TASK_UNBIND_FROM_PROJECT, "Unbind Task from Project");

    private static final Command TASK_LIST = new Command(CommandConst.TASK_LIST, "Show task list.");

    private static final Command TASK_LIST_BY_PROJECT_ID = new Command(CommandConst.TASK_LIST_BY_PROJECT_ID, "Show tasks list by project id.");

    private static final Command TASK_CREATE = new Command(CommandConst.TASK_CREATE, "Create new task.");

    private static final Command TASK_CLEAR = new Command(CommandConst.TASK_CLEAR, "Clear all tasks.");

    private static final Command EXIT = new Command(CommandConst.EXIT, "Close Application.");

    private static final Command[] COMMANDS = new Command[]{
            HELP, VERSION, ABOUT, INFO,

            PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR,
            PROJECT_SHOW_BY_ID, PROJECT_SHOW_BY_INDEX,
            PROJECT_REMOVE_BY_INDEX, PROJECT_REMOVE_BY_ID,
            PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX,
            PROJECT_CHANGE_STATUS_BY_ID, PROJECT_CHANGE_STATUS_BY_INDEX,
            PROJECT_START_BY_ID, PROJECT_START_BY_INDEX,
            PROJECT_COMPLETE_BY_ID, PROJECT_COMPLETE_BY_INDEX,

            TASK_LIST, TASK_CREATE, TASK_CLEAR,
            TASK_SHOW_BY_ID, TASK_SHOW_BY_INDEX,
            TASK_REMOVE_BY_INDEX, TASK_REMOVE_BY_ID,
            TASK_UPDATE_BY_INDEX, TASK_UPDATE_BY_ID,
            TASK_CHANGE_STATUS_BY_INDEX, TASK_CHANGE_STATUS_BY_ID,
            TASK_START_BY_INDEX, TASK_START_BY_ID,
            TASK_COMPLETE_BY_INDEX, TASK_COMPLETE_BY_ID,
            TASK_LIST_BY_PROJECT_ID, TASK_BIND_TO_PROJECT, TASK_UNBIND_FROM_PROJECT,

            EXIT
    };

    public Command[] getCommands() {
        return COMMANDS;
    }

}